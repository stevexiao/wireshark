/* packet-rmt-flute.h
 * Reliable Multicast Transport (RMT)
 * FLUTE Protocol Instantiation function definitions
 * Copyright 2012, Shiyuan Xiao <shiyuan.xiao@gmail.com>
 * RFC 3926, October 2004
 *
 * File Delivery over Unidirectional Transport (FLUTE):
 * ----------------------------------
 */
 
#ifndef __PACKET_RMT_FLUTE__
#define __PACKET_RMT_FLUTE__

#include "packet-rmt-common.h"
#include "packet-rmt-lct.h"
#include "packet-rmt-fec.h"

/* Type definitions */
/* ================ */

/* Logical FLUTE packet representation */
struct _flute
{
	guint8 version;
	struct _lct lct;
	struct _fec fec;
};

/* Wireshark stuff */
/* ============== */

/* FLUTE header field definitions*/
struct _flute_hf
{
	int version;
	
	struct _lct_hf lct;
	struct _fec_hf fec;
	
	int payload;
};

/* FLUTE subtrees */
struct _flute_ett
{
	gint main;
	
	struct _lct_ett lct;
	struct _fec_ett fec;
};

/* FLUTE preferences */
struct _flute_prefs
{
	gboolean use_default_udp_port;
	guint default_udp_port;

	struct _lct_prefs lct;
	struct _fec_prefs fec;
};

/* Function declarations */
/* ===================== */

#endif
