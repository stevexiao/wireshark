/* packet-rmt-flute.c
 * Reliable Multicast Transport (RMT)
 * FLUTE Protocol Instantiation function definitions
 * Copyright 2012, Shiyuan Xiao <shiyuan.xiao@gmail.com>
 * RFC 3926, October 2004
 *
 * File Delivery over Unidirectional Transport (FLUTE):
 * ----------------------------------
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include <epan/packet.h>
#include <epan/prefs.h>

#include "packet-rmt-flute.h"

/* Initialize the protocol and registered fields */
/* ============================================= */

static int proto = -1;

static struct _flute_hf hf;
static struct _flute_ett ett;

static struct _flute_prefs preferences;
static dissector_handle_t xml_handle;


/* Preferences */
/* =========== */

/* Set/Reset preferences to default values */
static void flute_prefs_set_default(struct _flute_prefs *flute_prefs)
{
	flute_prefs->use_default_udp_port = FALSE;
	flute_prefs->default_udp_port = 4001;

	lct_prefs_set_default(&flute_prefs->lct);
	fec_prefs_set_default(&flute_prefs->fec);
}

/* Register preferences */
static void flute_prefs_register(struct _flute_prefs *flute_prefs, module_t *module)
{
	prefs_register_bool_preference(module,
		"default.udp_port.enabled",
		"Use default UDP port",
		"Whether that payload of UDP packets with a specific destination port should be automatically dissected as FLUTE packets",
		 &flute_prefs->use_default_udp_port);

 	prefs_register_uint_preference(module,
		"default.udp_port",
		"Default UDP destination port",
		"Specifies the UDP destination port for automatic dissection of FLUTE packets",
		 10, &flute_prefs->default_udp_port);

	lct_prefs_register(&flute_prefs->lct, module);
	fec_prefs_register(&flute_prefs->fec, module);
}

/* Save preferences to flute_prefs_old */
static void flute_prefs_save(struct _flute_prefs *p, struct _flute_prefs *p_old)
{
	*p_old = *p;
}

/* Code to actually dissect the packets */
/* ==================================== */

static void dissect_flute(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
	/* Logical packet representation */
	struct _flute flute;

	/* Offset for subpacket dissection */
	guint offset;

	/* Set up structures needed to add the protocol subtree and manage it */
	proto_item *ti;
	proto_tree *flute_tree;

	/* Flute or not */
	tvbuff_t *new_tvb;
	gboolean is_flute = FALSE;

	/* Structures and variables initialization */
	offset = 0;
	memset(&flute, 0, sizeof(struct _flute));

	/* Update packet info */
	pinfo->current_proto = "FLUTE";

	/* Make entries in Protocol column and Info column on summary display */
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "FLUTE");
	col_clear(pinfo->cinfo, COL_INFO);

	/* FLUTE header dissection */
	/* --------------------- */

	flute.version = hi_nibble(tvb_get_guint8(tvb, offset));

	if (tree)
	{
		/* Create subtree for the FLUTE protocol */
		ti = proto_tree_add_item(tree, proto, tvb, offset, -1, FALSE);
		flute_tree = proto_item_add_subtree(ti, ett.main);

		/* Fill the FLUTE subtree */
		proto_tree_add_uint(flute_tree, hf.version, tvb, offset, 1, flute.version);

	} else
		flute_tree = NULL;

	/* This dissector supports only FLUTEv1 packets.
	 * If flute.version > 1 print only version field and quit.
	 */
	if (flute.version == 1) {

		struct _lct_ptr l;
		struct _fec_ptr f;

		l.lct = &flute.lct;
		l.hf = &hf.lct;
		l.ett = &ett.lct;
		l.prefs = &preferences.lct;

		f.fec = &flute.fec;
		f.hf = &hf.fec;
		f.ett = &ett.fec;
		f.prefs = &preferences.fec;

		/* LCT header dissection */
		/* --------------------- */

		is_flute = lct_dissector(l, f, tvb, flute_tree, &offset);

		/* FEC header dissection */
		/* --------------------- */

		/* Only if it's present and if LCT dissector has determined FEC Encoding ID
		 * FEC dissector should be called with fec->encoding_id* and fec->instance_id* filled
		 */
		if (flute.fec.encoding_id_present && tvb_length(tvb) > offset)
			fec_dissector(f, tvb, flute_tree, &offset);

		/* Add the Payload item */
		if (tvb_length(tvb) > offset){
			if(is_flute){
				new_tvb = tvb_new_subset_remaining(tvb,offset);
				call_dissector(xml_handle, new_tvb, pinfo, flute_tree);
			}else{
				proto_tree_add_none_format(flute_tree, hf.payload, tvb, offset, -1, "Payload (%u bytes)", tvb_length(tvb) - offset);
			}
		}

		/* Complete entry in Info column on summary display */
		/* ------------------------------------------------ */

		if (check_col(pinfo->cinfo, COL_INFO))
		{
			lct_info_column(&flute.lct, pinfo);
			fec_info_column(&flute.fec, pinfo);
		}

		/* Free g_allocated memory */
		lct_dissector_free(&flute.lct);
		fec_dissector_free(&flute.fec);

	} else {

		if (tree)
			proto_tree_add_text(flute_tree, tvb, 0, -1, "Sorry, this dissector supports FLUTE version 1 only");

		/* Complete entry in Info column on summary display */
		if (check_col(pinfo->cinfo, COL_INFO))
			col_add_fstr(pinfo->cinfo, COL_INFO, "Version: %u (not supported)", flute.version);
	}
}

void proto_reg_handoff_flute(void)
{
	static dissector_handle_t handle;
	static gboolean preferences_initialized = FALSE;
	static struct _flute_prefs preferences_old;

	if (!preferences_initialized)
	{
		preferences_initialized = TRUE;
		handle = create_dissector_handle(dissect_flute, proto);
		dissector_add_handle("udp.port", handle);
		xml_handle = find_dissector("xml");

	} else {

		if (preferences_old.use_default_udp_port)
			dissector_delete_uint("udp.port", preferences_old.default_udp_port, handle);
	}

	if (preferences.use_default_udp_port)
		dissector_add_uint("udp.port", preferences.default_udp_port, handle);

	flute_prefs_save(&preferences, &preferences_old);

}

void proto_register_flute(void)
{
	/* Setup FLUTE header fields */
	static hf_register_info hf_ptr[] = {

		{ &hf.version,
			{ "Version", "flute.version", FT_UINT8, BASE_DEC, NULL, 0x0, NULL, HFILL }},

		LCT_FIELD_ARRAY(hf.lct, "flute"),
		FEC_FIELD_ARRAY(hf.fec, "flute"),

		{ &hf.payload,
			{ "Payload", "flute.payload", FT_NONE, BASE_NONE, NULL, 0x0, NULL, HFILL }}
	};

	/* Setup protocol subtree array */
	static gint *ett_ptr[] = {
		&ett.main,

		LCT_SUBTREE_ARRAY(ett.lct),
		FEC_SUBTREE_ARRAY(ett.fec)
	};

	module_t *module;

	/* Clear hf and ett fields */
	memset(&hf, 0xff, sizeof(struct _flute_hf));
	memset(&ett, 0xff, sizeof(struct _flute_ett));

	/* Register the protocol name and description */
	proto = proto_register_protocol("File Delivery over Unidirectional Transport", "FLUTE", "flute");

	/* Register the header fields and subtrees used */
	proto_register_field_array(proto, hf_ptr, array_length(hf_ptr));
	proto_register_subtree_array(ett_ptr, array_length(ett_ptr));

	/* Reset preferences */
	flute_prefs_set_default(&preferences);

	/* Register preferences */
	module = prefs_register_protocol(proto, proto_reg_handoff_flute);
	flute_prefs_register(&preferences, module);

}
