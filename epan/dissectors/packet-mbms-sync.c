/* packet-mbms-sync.c
 * Multimedia Broadcast Multicast Service (MBMS)
 * Sync Protocol Instantiation function definitions
 * Copyright 2012, Shiyuan Xiao <shiyuan.xiao@gmail.com>
 * 3GPP TS 25.246, V9.2.1 (2011-03)
 *
 * MBMS synchronisation protocol (SYNC):
 * ----------------------------------
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <math.h>

#include <epan/packet.h>
#include <epan/prefs.h>
#include <epan/conversation.h>

#include "packet-mbms-sync.h"
#include "packet-mbms-crc.h"

/* Initialize the protocol and registered fields */
/* ============================================= */

static int sync_proto = -1;
static struct _sync_hf hf;
static struct _sync_ett ett;
static struct _sync_prefs preferences;

static dissector_handle_t ipv4_handle;
static dissector_handle_t ipv6_handle;
static dissector_handle_t data_handle;

static void reset_sync_sequence_info(struct _sync_sequence *info)
{
    memset(info, 0, sizeof(struct _sync_sequence));
    info->type1_latest_number = -1;
}

static guint64 ntime2utc(nstime_t* time)
{
    guint64 utcTime = (guint64)(time->secs*1000) + (guint64)(time->nsecs/1000000);

    return utcTime;
}

// calculate relative GPS time from UTC
// utcTime: ms
static guint16 UTC2SyncTimestamp(struct _sync_prefs *sync_prefs, guint64 utcTime)
{
    // UTC time + current leap seconds
    // 315964800000 1980.1.6 0:0 to 1970.1.1 0:0
    guint64 gpsTime = utcTime - 315964800000 + sync_prefs->leap_seconds*1000 - sync_prefs->system_timer_offset;
    guint seqCount = 0;

    if(sync_prefs->eNB)
    {
        // for eNB, sync_prefs->max_transmission_time should not be included
        seqCount = ((gpsTime - sync_prefs->common_reference_time + sync_prefs->msp)%sync_prefs->sync_period)/sync_prefs->sync_sequence_length;
    }
    else
    {
        seqCount = ((gpsTime - sync_prefs->common_reference_time + sync_prefs->max_transmission_time + sync_prefs->msp)%sync_prefs->sync_period)/sync_prefs->sync_sequence_length;
    }

    return (guint16)((seqCount*sync_prefs->sync_sequence_length)/10);
}

static guint16 getSyncTimestamp(struct _sync_prefs *sync_prefs, nstime_t* time)
{
    return UTC2SyncTimestamp(sync_prefs, ntime2utc(time));
}

/* Preferences */
/* =========== */

/* Set/Reset preferences to default values */
static void sync_prefs_set_default(struct _sync_prefs *sync_prefs)
{
    sync_prefs->use_default_udp_port = FALSE;
    sync_prefs->default_udp_port = 4001;
    sync_prefs->decode_payload = TRUE;
    sync_prefs->header_checksum = TRUE;
    sync_prefs->payload_checksum = TRUE;
    sync_prefs->payload_ipv6 = TRUE;
    sync_prefs->validate = TRUE;
    sync_prefs->eNB = FALSE;
    sync_prefs->first_packet_baseline = TRUE;
    sync_prefs->common_reference_time = 0;
    sync_prefs->leap_seconds = 16;
    sync_prefs->sync_sequence_length = 80; //ms
    sync_prefs->sync_period = 593920; //ms, must be multiple times of sync_sequence_length (7424x80ms)
    sync_prefs->max_sync_period = 20;
    sync_prefs->max_transmission_time = 0; //ms
    sync_prefs->msp = 0;
    sync_prefs->sync_type3_repeat = 2;
    sync_prefs->timestamp_tolerance = sync_prefs->sync_sequence_length; //ms
    sync_prefs->system_timer_offset = 0;
}

/* Register preferences */
static void sync_prefs_register(struct _sync_prefs *sync_prefs, module_t *module)
{
    prefs_register_bool_preference(module,
        "default.udp_port.enabled",
        "Use default UDP port",
        "Whether that payload of UDP packets with a specific destination port should be automatically dissected as SYNC packets",
        &sync_prefs->use_default_udp_port);

    prefs_register_uint_preference(module,
        "default.udp_port",
        "Default UDP destination port",
        "Specifies the UDP destination port for automatic dissection of SYNC packets",
        10, &sync_prefs->default_udp_port);

    prefs_register_bool_preference(module,
        "default.payload.decode",
        "Whether decode payload",
        "Specifies whether the payload of SYNC packets should be decoded",
        &sync_prefs->decode_payload);

    prefs_register_bool_preference(module, "default.header_checksum",
        "Validate SYNC header CRC check if possible",
        "Whether to check SYNC header CRC ",
        &sync_prefs->header_checksum);

    prefs_register_bool_preference(module, "default.payload_checksum",
        "Validate SYNC payload CRC check if possible",
        "Whether to check SYNC payload CRC ",
        &sync_prefs->payload_checksum);

    prefs_register_bool_preference(module,
        "default.payload.ipv6",
        "IPv6 for payload",
        "Specifies whether the payload of SYNC packets is using IPv6",
        &sync_prefs->payload_ipv6);

    prefs_register_bool_preference(module,
        "default.sync.validate",
        "Validate SYNC packet",
        "Specifies whether SYNC packet should be validated",
        &sync_prefs->validate);

    prefs_register_bool_preference(module,
        "default.sync.enb",
        "if the cap file is captured at eNB",
        "Specifies whether the cap file is captured at eNB",
        &sync_prefs->eNB);

    prefs_register_bool_preference(module,
        "default.sync.first_packet_baseline",
        "Use the timestamp in the first packet's header as baseline",
        "Specifies whether the timestamp in the first packet's header is used as baseline",
        &sync_prefs->first_packet_baseline);

    prefs_register_uint_preference(module,
        "default.sync.common_time_reference",
        "Common time reference(GPS absolute time)(ms)",
        "Specifies common time reference(GPS absolute time)",
        10, &sync_prefs->common_reference_time);

    prefs_register_uint_preference(module,
        "default.sync.leap_seconds",
        "Current leap seconds(s)",
        "Specifies current leap seconds",
        10, &sync_prefs->leap_seconds);

    prefs_register_uint_preference(module,
        "default.sync.sync_sequence_length",
        "Sync sequence length(ms)",
        "Specifies sync sequence length used by SYNC",
        10, &sync_prefs->sync_sequence_length);

    prefs_register_uint_preference(module,
        "default.sync.sync_period",
        "Sync period(ms)",
        "Specifies sync period used by SYNC which must be multiple times of sync sequence length",
        10, &sync_prefs->sync_period);

    prefs_register_uint_preference(module,
        "default.sync.max_sync_period",
        "Max count of sync periods in the capture file",
        "Specifies max count of sync periods in the capture file",
        10, &sync_prefs->max_sync_period);

    prefs_register_uint_preference(module,
        "default.sync.max_transmission_time",
        "Max transmission time(ms)",
        "Specifies max transmission time used by SYNC",
        10, &sync_prefs->max_transmission_time);

    prefs_register_uint_preference(module,
        "default.sync.msp",
        "MSP time(ms)",
        "Specifies the MSP parameter",
        10, &sync_prefs->msp);

    prefs_register_uint_preference(module,
        "default.sync.sync_type3_repeat",
        "SYNC type3 packet repeat times",
        "Specifies SYNC type3 packet repeat times in one sync sequence",
        10, &sync_prefs->sync_type3_repeat);

    prefs_register_uint_preference(module,
        "default.sync.timestamp_tolerance",
        "Timestamp tolerance for checking(ms)",
        "Specifies timestamp tolerance. If the timestamp gap is within it, we will think time stamp checking is passed",
        10, &sync_prefs->timestamp_tolerance);

    prefs_register_uint_preference(module,
        "default.sync.system_timer_offset",
        "The offset of capture file time(ms)",
        "Specifies the offset of capture file time. If it is set, the capture file time should be smaller.",
        10, &sync_prefs->system_timer_offset);
}

/* Save preferences to sync_prefs_old */
static void sync_prefs_save(struct _sync_prefs *p, struct _sync_prefs *p_old)
{
    *p_old = *p;
}

void sync_info_column(struct _sync *sync, packet_info *pinfo, guint recvTimestamp, 
    enum _sync_error err)
{    
    char err_msg[1024] = {0};

    if(preferences.validate)
    {
        if(err&error_timestamp)
        {
            sprintf(err_msg, "%s", "[timestamp error]");
        }

        if(err&error_misorder)
        {
            sprintf(err_msg, "%s%s", err_msg, "[misorder]");
        }

        if(err&error_badnumber)
        {
            sprintf(err_msg, "%s%s", err_msg, "[bad packet number]");
        }

        if(err&error_type1_loss)
        {
            sprintf(err_msg, "%s%s", err_msg, "[type 1 loss]");
        }

        if(err&error_type3_loss)
        {
            sprintf(err_msg, "%s%s", err_msg, "[type 3 loss]");
        }

        if(err&error_type1_toomany)
        {
            sprintf(err_msg, "%s%s", err_msg, "[too many type 1]");
        }

        if(err&error_type3_toomany)
        {
            sprintf(err_msg, "%s%s", err_msg, "[too many type 3]");
        }
    }    

    if(err == error_none || !preferences.validate)
    {
        col_prepend_fstr(pinfo->cinfo, COL_INFO, "TYPE:%u Timestamp:%u(%u) Num:%u ", 
            sync->type, sync->timestamp, recvTimestamp, sync->packetNum);
    }
    else
    {
        col_prepend_fstr(pinfo->cinfo, COL_INFO, "TYPE:%u Timestamp:%u(%u) Num:%u %s ", 
            sync->type, sync->timestamp, recvTimestamp, sync->packetNum, err_msg);
    }

    if(sync->type == 1)
    {
        col_prepend_fstr(pinfo->cinfo, COL_PROTOCOL, "SYNC/");
    }
}

/* Code to actually dissect the packets */
/* ==================================== */

static void dissect_sync(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    tvbuff_t *next_tvb = NULL;

	/* Logical packet representation */
	struct _sync sync;

	/* Offset for subpacket dissection */
	guint offset = 0;
    guint size = 0;

    guint maxPacketCount = 1200000; // max count of supported packets in capture file

    guint64 utcRecvTime = 0;

	/* Set up structures needed to add the protocol subtree and manage it */
	proto_item *ti=NULL;
    proto_item *item=NULL;
	proto_tree *sync_tree=NULL;
	
    guint8 computedHeaderChecksum = 0;
    guint16 computedPayloadChecksum = 0;
	guint16 checksum;
    gboolean odd;    
    guint16 index;
    
    guint8 headerBuff[128];
    guint8 headerLength;

    guint8* payload;
    guint32 payloadLen;

    conversation_t *conversation = NULL;
    struct _sync_conversation_data *conversation_info = NULL;

    struct _sync_sequence *sequence = NULL;
    struct _sync_sequence *previous_sequence = NULL;
    
    guint recv_timestamp = 0;
    enum _sync_error error = error_none;

	/* Structures and variables initialization */
	offset = 0;
	memset(&sync, 0, sizeof(struct _sync));

	/* Update packet info */
	pinfo->current_proto = "SYNC";

	/* Make entries in Protocol column and Info column on summary display */
	col_set_str(pinfo->cinfo, COL_PROTOCOL, "SYNC");
	col_clear(pinfo->cinfo, COL_INFO);	

    /* SYNC dissection */
    /* --------------------- */

    if (tree == NULL)
        return;

    /* Create subtree for the SYNC protocol */
    ti = proto_tree_add_item(tree, sync_proto, tvb, offset, -1, FALSE);
    sync_tree = proto_item_add_subtree(ti, ett.main);

    /* Fill the SYNC subtree */
    sync.type = hi_nibble(tvb_get_guint8(tvb, offset));
    proto_tree_add_uint(sync_tree, hf.type, tvb, offset, 1, sync.type);
    offset++;

    sync.timestamp = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(sync_tree, hf.timestamp, tvb, offset, 2, sync.timestamp);
    offset += 2;

    sync.packetNum = tvb_get_ntohs(tvb, offset);
    proto_tree_add_uint(sync_tree, hf.packetNum, tvb, offset, 2, sync.packetNum);
    offset += 2;

    sync.elapsedOctetCounter = tvb_get_ntohl(tvb, offset);
    proto_tree_add_uint(sync_tree, hf.elapsedOctetCounter, tvb, offset, 4, sync.elapsedOctetCounter);
    offset += 4;

    // find conversation
    conversation = find_conversation(pinfo->fd->num, &pinfo->src, &pinfo->dst,
        pinfo->ptype, pinfo->srcport, pinfo->destport, 0);

    if (conversation)
    {
        conversation_info = 
            (struct _sync_conversation_data*)conversation_get_proto_data(conversation, sync_proto);
    }
    else 
    {
        // new conversation create local data structure
        conversation_info = se_alloc(sizeof(struct _sync_conversation_data));

        // allocate memory for sequences in first sync period
        conversation_info->sync_sequence_count = preferences.sync_period/preferences.sync_sequence_length;

        size = preferences.max_sync_period*conversation_info->sync_sequence_count*sizeof(struct _sync_sequence);
        conversation_info->sequences = se_alloc(size);
        conversation_info->errors = se_alloc(maxPacketCount*sizeof(guint8));
        conversation_info->timestamps = se_alloc(maxPacketCount*sizeof(guint16));
        memset(conversation_info->sequences, 0, size);
        memset(conversation_info->errors, 0, maxPacketCount*sizeof(guint8));
        memset(conversation_info->timestamps, 0, maxPacketCount*sizeof(guint16));

        // setup the new data structure
        conversation_info->max_header_timestamp = (preferences.sync_period-preferences.sync_sequence_length)/10;
        conversation_info->setup_packet_number = pinfo->fd->num;

        conversation_info->first_packet_recv_time = ntime2utc(&pinfo->fd->abs_ts);
        conversation_info->first_packet_header_time = 
            315964800000 - preferences.leap_seconds*1000 + sync.timestamp*10; // use as baseline 

        conversation_info->packetNum = pinfo->fd->num - 1;
        conversation_info->sequenceIndex = 0;
        conversation_info->sequenceTimestamp = sync.timestamp; 

        // create the conversation with your data pointer
        conversation = conversation_new(pinfo->fd->num,  &pinfo->src, &pinfo->dst, pinfo->ptype,
            pinfo->srcport, pinfo->destport, 0);
        conversation_add_proto_data(conversation, sync_proto, (void*)conversation_info);
    }

    if( pinfo->fd->num > (maxPacketCount - 1) )
    {
        // too many packets
        return;
    }

    error = conversation_info->errors[pinfo->fd->num];
    recv_timestamp = conversation_info->timestamps[pinfo->fd->num];

    if( preferences.validate && 
        (pinfo->fd->num == conversation_info->packetNum + 1) )
    {
        //need to parse packet by order
        conversation_info->packetNum = pinfo->fd->num; 

        if(sync.timestamp != conversation_info->sequenceTimestamp)
        {
            if( sync.timestamp == 0 )
            {
                if(conversation_info->sequenceTimestamp != (preferences.sync_period - preferences.sync_sequence_length)/10)
                {
                    error |= error_timestamp;
                }                
            }
            else if(sync.timestamp != conversation_info->sequenceTimestamp+preferences.sync_sequence_length/10)
            {
                // not a continuous sequence
                error |= error_timestamp;
            }

            // another sync sequence
            conversation_info->sequenceIndex++;
            conversation_info->sequenceTimestamp = sync.timestamp;             
        }

        sequence = &conversation_info->sequences[conversation_info->sequenceIndex];
        sequence->header_timestamp = sync.timestamp;

        if( conversation_info->sequenceIndex > 0 )
        {
            previous_sequence = &conversation_info->sequences[conversation_info->sequenceIndex - 1];
        }
        else
        {
            previous_sequence = NULL;        
        }

        // convert received timestamp to SYNC timestamp
        utcRecvTime = ntime2utc(&pinfo->fd->abs_ts);
        
        if(preferences.first_packet_baseline)
        {
            utcRecvTime = (utcRecvTime - conversation_info->first_packet_recv_time) 
                + conversation_info->first_packet_header_time;            
        }

        recv_timestamp = UTC2SyncTimestamp(&preferences, utcRecvTime);

        conversation_info->timestamps[pinfo->fd->num] = recv_timestamp;
    }

    switch(sync.type)
    {    
    case 1:
        {
            headerLength = offset;
            tvb_memcpy(tvb, headerBuff, 0, headerLength);

            if(preferences.header_checksum)
            {
                computedHeaderChecksum = crc6_compute(headerBuff, headerLength);
            }

            checksum = tvb_get_ntohs(tvb, offset);
            sync.headerCRC =  (checksum & 0xFC00) >> 10;
            sync.payloadCRC = (checksum & 0x03FF);         

            payloadLen = tvb_length_remaining(tvb, offset + 2);
            payload = tvb_get_ptr(tvb, offset + 2, payloadLen);

            if(preferences.payload_checksum)
            {
                computedPayloadChecksum = crc10_compute(payload, payloadLen);
            }

            if(!preferences.header_checksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Check disabled]", sync.headerCRC);
            }        
            else if(sync.headerCRC == computedHeaderChecksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Correct]", sync.headerCRC);
            }
            else
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Incorrect, shoulde be: 0x%02x]", 
                    sync.headerCRC, computedHeaderChecksum);
            }

            //verify checksum
            if(!preferences.payload_checksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Check disabled]", sync.payloadCRC);
            }
            else if(sync.payloadCRC == computedPayloadChecksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Correct]", sync.payloadCRC);
            }        
            else
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Incorrect, shoulde be: 0x%04x]", 
                    sync.payloadCRC, computedPayloadChecksum);
            }

            // check timestamp
            if( preferences.validate && (pinfo->fd->num == conversation_info->packetNum) && sequence )
            {
                if(previous_sequence && !previous_sequence->terminated)
                {
                    //handle previous sequence
                    if(previous_sequence->type3_recv_count < preferences.sync_type3_repeat)
                    {
                        // Some type 3 packets in previous sequence are lost
                        error |= error_type3_loss;

                        previous_sequence->terminated = TRUE;
                    } 

                    if(sync.timestamp != 0 && sync.timestamp < previous_sequence->header_timestamp)
                    {
                        error |= error_misorder;
                    }
                }                

                if(sync.packetNum < sequence->type1_latest_number)
                {
                    // misorder, received packet in same sequence
                    error |= error_misorder;
                }                

                sequence->type1_recv_count++;
                sequence->type1_latest_number = sync.packetNum;

                if(sync.packetNum == 0)
                {
                    // first type1 packet should not be sent late but can be sent a little earlier
                    if(sequence->header_timestamp == conversation_info->max_header_timestamp)
                    {
                        //recv_timestamp can be a little earlier or equal to sequence->header_timestamp
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < sequence->header_timestamp - preferences.timestamp_tolerance/10)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }
                        else
                        {
                            if(recv_timestamp < preferences.sync_sequence_length && recv_timestamp >= 0)
                            {
                                // too late
                                error |= error_timestamp;
                            }
                        }
                    }
                    else if(sequence->header_timestamp == 0)
                    {
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < conversation_info->max_header_timestamp)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }                        
                    }
                    else
                    {
                        if( (recv_timestamp > sequence->header_timestamp) || 
                            recv_timestamp < sequence->header_timestamp - preferences.timestamp_tolerance/10)
                        {
                            error |= error_timestamp;
                        }
                    }                    
                }
                else 
                {
                    if(sequence->header_timestamp == conversation_info->max_header_timestamp)
                    {
                        if(recv_timestamp > sequence->header_timestamp - preferences.sync_sequence_length
                            && recv_timestamp < sequence->header_timestamp - preferences.timestamp_tolerance/10)
                        {
                            error |= error_timestamp;
                        }
                        else if(recv_timestamp < preferences.sync_sequence_length 
                            && recv_timestamp > preferences.timestamp_tolerance/10)
                        {                            
                            error |= error_timestamp;
                        }     
                    }
                    else if(sequence->header_timestamp == 0)
                    {
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < conversation_info->max_header_timestamp)
                        {
                            error |= error_timestamp;
                        }
                        else if(recv_timestamp < preferences.sync_sequence_length 
                            && recv_timestamp > preferences.timestamp_tolerance/10)
                        {
                            error |= error_timestamp;
                        }
                    }
                    else
                    {                        
                        if(abs(recv_timestamp - sequence->header_timestamp) > preferences.timestamp_tolerance/10)
                        {
                            error |= error_timestamp;
                        }    
                    }                                    
                }  

                conversation_info->errors[pinfo->fd->num] = error;               
            }             

            if( preferences.validate )
            {
                item = proto_tree_add_boolean(sync_tree, hf.validate, NULL,
                    0, 0, error == error_none);
                PROTO_ITEM_SET_GENERATED(item);
            }

            // parse payload
            offset += 2;  // 2bytes for checksum            
            next_tvb = tvb_new_subset_remaining(tvb, offset);

            if(preferences.decode_payload)
            {
                if(preferences.payload_ipv6)
                {
                    call_dissector(ipv6_handle, next_tvb, pinfo, tree);
                }
                else
                {
                    call_dissector(ipv4_handle, next_tvb, pinfo, tree);
                }
            }
            else
            {
                call_dissector(data_handle, next_tvb, pinfo, tree);
            }
        }        
        break;
    case 3:
        {
            sync.totalNumOfPacket = tvb_get_ntoh24(tvb, offset);
            proto_tree_add_uint(sync_tree, hf.totalNumOfPacket, tvb, offset, 3, sync.totalNumOfPacket);
            offset += 3;        

            sync.totalNumOfOctet = tvb_get_ntoh40(tvb, offset);
            proto_tree_add_uint64(sync_tree, hf.totalNumOfOctet, tvb, offset, 5, sync.totalNumOfOctet);
            offset += 5; 

            headerLength = offset;
            tvb_memcpy(tvb, headerBuff, 0, headerLength);

            if(preferences.header_checksum)
            {
                computedHeaderChecksum = crc6_compute(headerBuff, headerLength);
            }

            checksum = tvb_get_ntohs(tvb, offset);
            sync.headerCRC =  (checksum & 0xFC00) >> 10;
            sync.payloadCRC = (checksum & 0x03FF);

            payloadLen = tvb_length_remaining(tvb, offset + 2);
            payload = tvb_get_ptr(tvb, offset + 2, payloadLen);

            if(preferences.payload_checksum)
            {
                computedPayloadChecksum = crc10_compute(payload, payloadLen);
            }

            if(!preferences.header_checksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Check disabled]", sync.headerCRC);
            }        
            else if(sync.headerCRC == computedHeaderChecksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Correct]", sync.headerCRC);
            }
            else
            {
                proto_tree_add_uint_format(sync_tree, hf.headerCRC, tvb, offset, 1, 
                    sync.headerCRC, "Header CRC: 0x%02x [Incorrect, shoulde be: 0x%02x]", 
                    sync.headerCRC, computedHeaderChecksum);
            }

            //verify checksum
            if(!preferences.payload_checksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Check disabled]", sync.payloadCRC);
            }
            else if(sync.payloadCRC == computedPayloadChecksum)
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Correct]", sync.payloadCRC);
            }        
            else
            {
                proto_tree_add_uint_format(sync_tree, hf.payloadCRC, tvb, offset, 2, 
                    sync.payloadCRC, "Payload CRC: 0x%04x [Incorrect, shoulde be: 0x%04x]", 
                    sync.payloadCRC, computedPayloadChecksum);
            }

            
            offset += 2;  // 2bytes for checksum        

            for(index = 1, odd = TRUE; index < sync.packetNum; index++)
            {
                if(odd)
                {
                    tvb_get_ntohs(tvb, offset);
                } 
                else
                {
                    tvb_get_guint8(tvb, offset);
                }

                odd = !odd;
            }  
            
            // check timestamp
            if( preferences.validate && (pinfo->fd->num == conversation_info->packetNum) && sequence )
            {                
                if(previous_sequence && !previous_sequence->terminated)
                {
                    // handle previous sequence
                    if(previous_sequence->type3_recv_count < preferences.sync_type3_repeat)
                    {
                        // Some type 3 packets in previous sequence are lost
                        error |= error_type3_loss;

                        previous_sequence->terminated = TRUE;
                    } 
                }
                
                sequence->type3_recv_count++;

                if(sequence->type1_recv_count < sync.packetNum)
                {
                    error |= error_type1_loss;
                }

                if(sequence->type3_recv_count > preferences.sync_type3_repeat)
                {
                    error |= error_type3_toomany;
                }

                if(sequence->type3_recv_count == 1 && sync.packetNum == 0)
                {
                    // the first packet in the sync sequence
                    // first packet should not be sent late but can be sent a little earlier

                    if(sequence->header_timestamp == conversation_info->max_header_timestamp)
                    {
                        //recv_timestamp can be a little earlier or equal to sequence->header_timestamp
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < sequence->header_timestamp - preferences.timestamp_tolerance/10)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }
                        else
                        {
                            if(recv_timestamp < preferences.sync_sequence_length && recv_timestamp >= 0)
                            {
                                // too late
                                error |= error_timestamp;
                            }
                        }
                    }
                    else if(sequence->header_timestamp == 0)
                    {
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < conversation_info->max_header_timestamp)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }
                        else 
                        {
                            if(recv_timestamp < preferences.sync_sequence_length && recv_timestamp >= 0)
                            {
                                // too late
                                error |= error_timestamp;
                            } 
                        }
                    }
                    else if(recv_timestamp > sequence->header_timestamp)
                    {
                        // too late
                        error |= error_timestamp;
                    }                       
                }
                else
                {
                    if(sequence->header_timestamp == conversation_info->max_header_timestamp)
                    {
                        //recv_timestamp can be a little smaller or equal to sequence->header_timestamp or 0
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < sequence->header_timestamp - preferences.timestamp_tolerance/10)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }

                        if(recv_timestamp != sequence->header_timestamp && recv_timestamp > preferences.timestamp_tolerance/10)
                        {
                            // too late
                            error |= error_timestamp;
                        }     
                    }
                    else if(sequence->header_timestamp == 0)
                    {
                        if(recv_timestamp > preferences.sync_period/10 - preferences.sync_sequence_length
                            && recv_timestamp < conversation_info->max_header_timestamp)
                        {
                            // too earlier
                            error |= error_timestamp;
                        }
                        else if(recv_timestamp < preferences.sync_sequence_length 
                            && recv_timestamp > preferences.timestamp_tolerance/10)
                        {
                            // too late
                            error |= error_timestamp;
                        } 
                    }
                    else if(abs(recv_timestamp - sequence->header_timestamp) > preferences.timestamp_tolerance/10)
                    {
                        error |= error_timestamp;
                    }   
                }
                

                conversation_info->errors[pinfo->fd->num] = error;
            } 

            if( preferences.validate )
            {
                item = proto_tree_add_boolean(sync_tree, hf.validate, NULL,
                    0, 0, error == error_none);
                PROTO_ITEM_SET_GENERATED(item);
            }
        }
        break;
    }    

    sync_info_column(&sync, pinfo, recv_timestamp, error);
}

void proto_reg_handoff_sync(void)
{
    static dissector_handle_t handle;
    static gboolean preferences_initialized = FALSE;
    static struct _sync_prefs preferences_old;

    ipv4_handle = find_dissector("ip");
    ipv6_handle = find_dissector("ipv6");
    data_handle = find_dissector("data");    

    if (!preferences_initialized)
    {
        preferences_initialized = TRUE;
        handle = create_dissector_handle(dissect_sync, sync_proto);
        dissector_add_handle("udp.port", handle);
    } else {

        if (preferences_old.use_default_udp_port)
            dissector_delete_uint("udp.port", preferences_old.default_udp_port, handle);
    }

    if (preferences.use_default_udp_port)
        dissector_add_uint("udp.port", preferences.default_udp_port, handle);

    sync_prefs_save(&preferences, &preferences_old);
}

void proto_register_sync(void)
{
    /* Setup SYNC header fields */
    static hf_register_info hf_ptr[] = {

        { &hf.type,
        { "PDU Type", "sync.type", FT_UINT8, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.timestamp,
        { "Time Stamp", "sync.timestamp", FT_UINT16, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.packetNum,
        { "Packet Number", "sync.packetNum", FT_UINT16, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.elapsedOctetCounter,
        { "Elapsed Octet Counter", "sync.elapsedOctetCounter", FT_UINT32, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.totalNumOfPacket,
        { "Total Number Of Packet", "sync.totalNumOfPacket", FT_UINT32, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.totalNumOfOctet,
        { "Total Number Of Octet", "sync.totalNumOfOctet", FT_UINT64, BASE_DEC, NULL, 0x0, NULL, HFILL }},

        { &hf.headerCRC,
        { "Header CRC", "sync.headerCRC", FT_UINT8, BASE_HEX, NULL, 0x0, NULL, HFILL }},

        { &hf.payloadCRC,
        { "Payload CRC", "sync.payloadCRC", FT_UINT16, BASE_HEX, NULL, 0x0, NULL, HFILL }},

        { &hf.validate,
        { "Validation",	"sync.validation", FT_BOOLEAN, BASE_NONE, NULL, 0x0,
        "SYNC packet validation", HFILL }},

        { &hf.payload,
        { "Payload", "sync.payload", FT_NONE, BASE_NONE, NULL, 0x0, NULL, HFILL }}
    };

    /* Setup protocol subtree array */
    static gint *ett_ptr[] = 
    {
        &ett.main
    };

    module_t *module;

    /* Clear hf fields */
    memset(&hf, 0xff, sizeof(struct _sync_hf));
    memset(&ett, 0xff, sizeof(struct _sync_ett));

    /* Register the protocol name and description */
    sync_proto = proto_register_protocol("MBMS Synchronisation Protocol", "SYNC", "sync");

    /* Register the header fields and subtrees used */
    proto_register_field_array(sync_proto, hf_ptr, array_length(hf_ptr));
    proto_register_subtree_array(ett_ptr, array_length(ett_ptr));

    /* Reset preferences */
    sync_prefs_set_default(&preferences);

    /* Register preferences */
    module = prefs_register_protocol(sync_proto, proto_reg_handoff_sync);
    sync_prefs_register(&preferences, module);

    crc10_buildtable();
}