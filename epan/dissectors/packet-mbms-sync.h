/* packet-mbms-sync.h
 * Multimedia Broadcast Multicast Service (MBMS)
 * Sync Protocol Instantiation function definitions
 * Copyright 2012, Shiyuan Xiao <shiyuan.xiao@gmail.com>
 * 3GPP TS 25.246, V9.2.1 (2011-03)
 *
 * MBMS synchronisation protocol (SYNC):
 * ----------------------------------
 */
 
#ifndef __PACKET_MBMS_SYNC__
#define __PACKET_MBMS_SYNC__

struct _sync
{
    guint8 type; /* PDU type */
    gboolean ipv6;
    guint16 timestamp;
    guint16 packetNum; // start from 0
    guint32 elapsedOctetCounter;
    guint32 totalNumOfPacket;
    guint64 totalNumOfOctet;
    guint8 headerCRC;
    guint16 payloadCRC;
    guint32 spareExtension;
};

/* SYNC header field definitions*/
struct _sync_hf
{
    int type;
    int timestamp;
    int packetNum; 
    int elapsedOctetCounter;
    int totalNumOfPacket;
    int totalNumOfOctet;
    int headerCRC;
    int payloadCRC;
    int validate;
    int payload;
};

/* SYNC subtrees */
struct _sync_ett
{
    gint main;
};

/* SYNC preferences */
struct _sync_prefs
{
    gboolean use_default_udp_port;
    guint    default_udp_port;
    gboolean decode_payload;
    gboolean header_checksum;
    gboolean payload_checksum;
    gboolean payload_ipv6;
    gboolean validate;              // flag for validating SYNC packet
    gboolean eNB;                   // flag for captured at eNB
    gboolean first_packet_baseline; // use the timestamp in the first packet's header as baseline
    guint    common_reference_time; 
    guint    leap_seconds;
    guint    sync_sequence_length;  // ms
    guint    sync_period;           // ms, must be multiple times of sync_sequence_length 
    guint    max_sync_period;       // max count of sync periods in capture file     
    guint    max_transmission_time; // ms
    guint    msp;                   // ms
    guint    sync_type3_repeat;     // the number of SYNC type 3 packets which should be sent in one sync_sequence
    guint    timestamp_tolerance;   // ms, the acceptable tolerance for timestamp check
    gint     system_timer_offset;   // ms, could be minus
};

enum _sync_error
{
    error_none = 0,
    error_timestamp=0x01,
    error_misorder=0x02,
    error_badnumber=0x04,
    error_type1_loss=0x08,
    error_type3_loss=0x10,
    error_type1_toomany=0x20,
    error_type3_toomany=0x40
};

// support max 2048 type 1 packet in one sync sequence
struct _sync_sequence
{
    gboolean terminated;                 // if sync sequence is terminated
    guint8   type1_recv_count;           // the number of received type 1 packets
    guint8   type3_recv_count;           // the number of received type 3 packets
    guint16  header_timestamp;           // the timestamp in header (GPS time) for this sync sequence
    gint16   type1_latest_number;        // the packet number of the latest type 1 packet in one sync sequence
};

struct _sync_conversation_data
{
    guint max_header_timestamp;           // max header timestamp sync period
    guint sync_sequence_count;            // the count of sync sequence in one 
    guint setup_packet_number;	          // the packet where this conversation is started
    
    guint64 first_packet_recv_time;       // UTC receiving time for first received packet
    guint64 first_packet_header_time;     // UTC time from header timestamp of first received packet
    
    guint   packetNum;
    guint   sequenceIndex;
    guint16 sequenceTimestamp;            // header timestamp

    guint8  *errors;                      // record error status of each packet
    guint16 *timestamps;                  // record timestamp of each packet
    
    struct _sync_sequence *sequences;     // max support 5 sync period
};

#endif