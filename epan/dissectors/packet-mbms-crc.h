/* packet-mbms-crc.h
 * Multimedia Broadcast Multicast Service (MBMS)
 * Sync Protocol Instantiation function definitions
 * Copyright 2012, Shiyuan Xiao <shiyuan.xiao@gmail.com>
 * 3GPP TS 25.246, V9.2.1 (2011-03)
 *
 * MBMS synchronisation protocol (SYNC):
 * CRC-6 and CRC-10
 * ----------------------------------
 */
 
#ifndef __PACKET_MBMS_CRC__
#define __PACKET_MBMS_CRC__

void crc10_buildtable();
unsigned short crc10_compute(unsigned char * data_blk_ptr, int data_blk_size);

unsigned char crc6_compute(unsigned char* buff, unsigned int buffLength);

#endif